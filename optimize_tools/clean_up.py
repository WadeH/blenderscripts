
def activate_obj(obj):
	"""
	Change active object in blender scene
	@param obj Object to make active
	"""
	bpy.context.scene.objects.active = obj


def remove_double_verts(obj):
	"""
	Remove double vertices from object
	@param obj Object to operate on
	@return Number of verts removed
	"""


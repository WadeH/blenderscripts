import bpy
import optimize_tools
import opt_script
import bquery

__name__ = "naming_tools"

def autoname_group_items(group_name, base_name, rename_mesh=False, compare_axis='x', reverse=False):
    b_objs = bpy.data.groups[group_name].objects
    sorted_obj_names = bquery.obj_tools.sort_by_position(b_objs, compare_axis, reverse)
    objs = list(map(bquery.object_by_name, sorted_obj_names))
    count = 1
    num_length = len(str(len(objs)))
    for o in objs:
        num = str(count)
        num = num.zfill(num_length)
        new_name = base_name + "_" + num
        mesh_name = new_name + ".mesh"
        print("Renaming " + o.name + " to " + new_name)
        o.name = new_name
        count = count + 1
        m = o.data
        if rename_mesh:
            m.name = mesh_name

#autoname_group_items('Rocks','Rock', True, 'x', True)

    
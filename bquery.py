import bpy
import vert_tools
import obj_tools

__name__ = "bquery"

def mesh_by_name(mesh_name):
    """ Find mesh by name
        mesh_name - name of mesh
    """
    return bpy.data.meshes[mesh_name]

def object_by_name(object_name):
    """ Find object by name
        object_name - Name of object
    """
    return bpy.data.objects[object_name]

def objects_by_group(group_name):
    """ Find objects by group name
        group_name - Name of group
    """
    return bpy.data.groups[group_name].objects

# optimization tools ~ blender

# """
# 	getting a group:
# 	bpy.data.groups[name].objects
# """

import bpy
import math

__name__ = "optimize_tools"

def is_in(query,collection):
	"""
	Returns true if query is in collection
	"""
	for item in collection:
		if item == query:
			return True
	return False

def activate_obj(obj):
	"""
	Change active object in blender scene
	@param obj Object to make active
	"""
	# make sure object mode is set
	bpy.ops.object.mode_set(mode='OBJECT')
	bpy.context.scene.objects.active = obj

def enter_edit_mode(enable=True):
	"""
	Enter vertex edit mode
	@param enable If False, exit edit mode
	"""
	if enable:
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_mode(type='VERT')
	else:
		# exit edit mode, return to object mode
		bpy.ops.object.mode_set(mode='OBJECT')


def remove_double_verts(obj, flatten_faces = True):
	"""
	Remove double vertices from object
	@param obj Object to operate on
	@param flatten_faces If True, remove smoothing from faces
	"""
	activate_obj(obj)
	enter_edit_mode()
	bpy.ops.mesh.remove_doubles()
	if flatten_faces:
		bpy.ops.mesh.faces_shade_flat()


def group_remove_doubles(group_name, flatten_faces = True):
	objs = bpy.data.groups[group_name].objects
	for o in objs:
		print("Removing doubles from " + o.name)
		remove_double_verts(o, flatten_faces)


def deg2rad(deg):
	return (deg/180.0)*math.pi


def add_decimation(obj, angle=30.0, delimit={'UV'}, name='Decimate'):
	"""
	Add decimation modifier to object
	@param obj Object
	@param angle Minimum angle (degrees)
	@param delimit Delimitor list
	@param name Name to assign to created modifier
	"""
	ang=deg2rad(angle)
	activate_obj(obj)

	# Only add modifier if one with the given name isn't already present
	# prevents duplicates, plus only the one with the right name
	# gets updated anywya ^_^
	if not is_in(name, bpy.context.object.modifiers.keys()):
		bpy.ops.object.modifier_add(type='DECIMATE')
	modref = bpy.context.object.modifiers["Decimate"]
	modref.name = name
	modref.decimate_type = 'DISSOLVE'
	modref.delimit = delimit
	modref.angle_limit = ang


def group_add_decimation(group_name, angle=30.0, delimit={'UV'}, name='Autodeci'):
	"""
	Apply decimation to all objects with group name
	@param group_name Name of group to apply to
	@param angle Minimum angle in degrees
	@param delimit Delimit options
	@param name Modifier name
	"""
	for obj in bpy.data.groups[group_name].objects:
		print("Decimating " + obj.name)
		add_decimation(obj, angle, delimit, name)

def group_apply_decimation(group_name, name='Autodeci'):
	for obj in bpy.data.groups[group_name].objects:
		activate_obj(obj)
		#if is_in(name, obj.modifiers.keys()):
		if is_in(name, bpy.context.object.modifiers.keys()):
			bpy.ops.object.modifier_apply(apply_as='DATA', modifier=name)

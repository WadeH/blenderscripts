import vert_tools

__name__ = "obj_tools"

def sort_by_position(objects, axis, reverse=False):
    """ Returns list of object names sorted by position on an axis
        objects - collection of objects
        axis - name of axis to compare on
        reverse - reverse results if True; False by default
    """
    axis_pos = dict()
    for obj in objects:
        ov = vert_tools.verts_by_object(obj)
        mv = vert_tools.min_vertex(ov, axis)
        axis_pos[obj.name] = mv
    sorted_objs = sorted(axis_pos, key=axis_pos.get, reverse=reverse)
    return sorted_objs
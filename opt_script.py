
import bpy
import optimize_tools

__name__ = "opt_script"

def is_in(query,collection):
    """
    Returns true if query is in collection
    """
    for item in collection:
        if item == query:
            return True
    return False

def group_names(exclude=[]):
    names = []
    for g in bpy.data.groups:
        names.append(g.name)
    # if there are some items to exclude, check them out!
    if len(exclude) > 0:
        for n in names:
            if is_in(n, exclude):
                names.remove(n)
    return names


def remove_doubles(targets=[]):
    if len(targets) == 0:
        targets = group_names()
    for target in targets:
        optimize_tools.group_remove_doubles(target)
    #optimize_tools.group_remove_doubles('Rocks', False)


def add_decim(excluded=['Shoji']):
    print(group_names())
    targets = group_names(excluded)
    for target in targets:
        optimize_tools.group_add_decimation(target)

#remove_doubles()
#add_decim


def apply_decim(excluded=['Rocks'], included=[]):
    if len(included) > 0:
        targets = included
    else:
        targets = group_names(excluded)
    for target in targets:
        optimize_tools.group_apply_decimation(target)


#remove_doubles()
#add_decim()
#apply_decim(
#optimize_tools.group_apply_decimation('FloorBoards')
__name__ = "vert_tools"

def __map_vertex__(v): return {'x': v[1].co.x, 'y': v[1].co.y, 'z': v[1].co.z}

def min_vertex(verts, axis):
    """ Get minimum vertex from list by axis
    verts - List of Vector objects
    axis - name of axis to sort by
    """
    vdict = verts_dict(verts)
    v = sorted(vdict, key=lambda k:k[axis])
    return v[0][axis]

def verts_dict(verts):
    return list(map(__map_vertex__, verts))

def verts_by_object(object):
    """ Return a list of all the vertices within an object's mesh
        object - object to pull vertices from
    """
    verts = object.data.vertices.items()
    return verts
